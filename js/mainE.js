document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var options = {
    	fullWidth: true,
    	indicators: true
    }
    var instances = M.Carousel.init(elems, options);
    console.log(instances);
    setInterval(function() {
        instances[0].next();
    }, 10000);

    
    elems = document.querySelectorAll('.dropdown-trigger');
    options = {
    	coverTrigger: false
    }
    M.Dropdown.init(elems, options);

  });


